package com.feb31.puzzler;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.content.Intent;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {



    ImageView imgBtn1;
    ImageView imgBtn2;
    ImageView imgBtn3;
    ImageView imgBtn4;
    ImageView imgBtn1_2;
    ImageView imgBtn2_2;
    ImageView imgBtn3_2;
    ImageView imgBtn4_2;

    ImageView imgHeartInside;

    AnimatorSet setRightOut;
    AnimatorSet setLeftIn;

    //heartanim
    Animation fadeOut;
    Animation shaker;

    //==//
    MediaPlayer mPlayer1;
    MediaPlayer mPlayer2;
    MediaPlayer mPlayer3;
    MediaPlayer mPlayer4;
    MediaPlayer mPlayer5;
    MediaPlayer mPlayer6;
    MediaPlayer mPlayer7;
    MediaPlayer mPlayer8;
    MediaPlayer mPlayer9;


    //==

    ImageView[] imgs = new ImageView[4];


    boolean isBackVisible1 = false; // Boolean variable to check if the back image is visible currently
    boolean isBackVisible2 = false; // Boolean variable to check if the back image is visible currently
    boolean isBackVisible3 = false; // Boolean variable to check if the back image is visible currently
    boolean isBackVisible4 = false; // Boolean variable to check if the back image is visible currently

    ArrayList<Integer> pictures;
    ArrayList<Integer> picturesM;
    ArrayList<MediaPlayer> mps;

    int [] masivi;

    int pictureIndex=-1;
    int mogeba = 0;
    int wageba=0;
    Boolean heartIsTaken=false;

    int request_Code=1; // mogeba
    int getRequest_Code_wageba=2;
    int guli=1;

    Intent intent;

     int level;
    SharedPreferences prefs;

    // WAGEBA
    Intent intentLOSE ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        prefs = getSharedPreferences("levels", MODE_PRIVATE);

          level=prefs.getInt("level", 1);; //=====================================================


        //====================================== heart   inside

        imgHeartInside=(ImageView)findViewById(R.id.heartinside);
        imgHeartInside.setVisibility(View.GONE);


        fadeOut  = AnimationUtils.loadAnimation(this, R.anim.setter);
        shaker  = AnimationUtils.loadAnimation(this, R.anim.shaker);


        //======================================

        //------------------------  musika



         mPlayer1 = MediaPlayer.create(this, R.raw.f1);
         mPlayer2 = MediaPlayer.create(MainActivity.this, R.raw.f2);;
         mPlayer3 = MediaPlayer.create(MainActivity.this, R.raw.f3);
         mPlayer4 = MediaPlayer.create(MainActivity.this, R.raw.f4);
         mPlayer5 = MediaPlayer.create(MainActivity.this, R.raw.f5);
         mPlayer6 = MediaPlayer.create(MainActivity.this, R.raw.f6);
         mPlayer7 = MediaPlayer.create(MainActivity.this, R.raw.f7);
         mPlayer8 = MediaPlayer.create(MainActivity.this, R.raw.f8);
         mPlayer9 = MediaPlayer.create(MainActivity.this, R.raw.f9);

        mPlayer1.setVolume(45,49);
        mPlayer2.setVolume(45,49);
        mPlayer3.setVolume(45,49);
        mPlayer4.setVolume(45,49);
        mPlayer5.setVolume(45,49);
        mPlayer6.setVolume(45,49);
        mPlayer7.setVolume(45,49);
        mPlayer8.setVolume(45,49);
        mPlayer9.setVolume(45,49);


        mps=new ArrayList<MediaPlayer>();
        mps.add(mPlayer1);
        mps.add(mPlayer2);
        mps.add(mPlayer3);
        mps.add(mPlayer4);
        mps.add(mPlayer5);
        mps.add(mPlayer6);
        mps.add(mPlayer7);
        mps.add(mPlayer8);
        mps.add(mPlayer9);
        //-----------------------


        // final AnimatorSet
        setRightOut = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.flight_right_out);

        setLeftIn = (AnimatorSet) AnimatorInflater.loadAnimator(this,
                R.animator.flight_left_in);

         // final Intent intent = new Intent(this, WinnerActivity.class);
        intent = new Intent(this, WinnerActivity.class);

        masivi=new int[4];
        masivi[0]=R.drawable.nihil1;
        masivi[1]=R.drawable.bomb2;
        masivi[2]=R.drawable.heart3;
        masivi[3]=R.drawable.bg;


//        pictures=new ArrayList<Integer>();
//        pictures.add(R.drawable.win1);
//        pictures.add(R.drawable.win2);
//        pictures.add(R.drawable.win3);
//        pictures.add(R.drawable.win4);

//        picturesM=new ArrayList<Integer>();
//        picturesM.add(R.drawable.p1);
//        picturesM.add(R.drawable.p2);
//        picturesM.add(R.drawable.p3);
//        picturesM.add(R.drawable.p4);


        // 9 iani
//        pictures=new ArrayList<Integer>();
//        pictures.add(R.drawable.winwin1);
//        pictures.add(R.drawable.winwin2);
//        pictures.add(R.drawable.winwin3);
//        pictures.add(R.drawable.winwin4);
//        pictures.add(R.drawable.winwin5);
//        pictures.add(R.drawable.winwin6);
//        pictures.add(R.drawable.winwin7);
//        pictures.add(R.drawable.winwin8);
//        pictures.add(R.drawable.winwin9);
//
//        picturesM=new ArrayList<Integer>();
//        picturesM.add(R.drawable.pp1);
//        picturesM.add(R.drawable.pp3);
//        picturesM.add(R.drawable.pp4);
//        picturesM.add(R.drawable.pp5);
//        picturesM.add(R.drawable.pp8);
//        picturesM.add(R.drawable.pp9);




        /////===== pirveli
        pictures=new ArrayList<Integer>();
        pictures.add(R.drawable.t1);
        pictures.add(R.drawable.t2);
        pictures.add(R.drawable.t3);
        pictures.add(R.drawable.t4);
        pictures.add(R.drawable.t5);
        pictures.add(R.drawable.t6);
        pictures.add(R.drawable.t7);
//        picturesM.add(R.drawable.pp6);
//        picturesM.add(R.drawable.pp7);
        pictures.add(R.drawable.t8);
        pictures.add(R.drawable.t9);
        pictures.add(R.drawable.t10);
        pictures.add(R.drawable.t11);
        pictures.add(R.drawable.t12);

//        picturesM.add(R.drawable.pp2);

        picturesM=new ArrayList<Integer>();
        picturesM.add(R.drawable.nasha1);
        picturesM.add(R.drawable.nasha2);
        picturesM.add(R.drawable.nasha3);
        picturesM.add(R.drawable.nasha4);
        picturesM.add(R.drawable.nasha5);
        picturesM.add(R.drawable.nasha6);
        picturesM.add(R.drawable.nasha7);
        picturesM.add(R.drawable.nasha8);
        picturesM.add(R.drawable.nasha9);
        picturesM.add(R.drawable.nasha10);
        picturesM.add(R.drawable.nasha11);
        picturesM.add(R.drawable.nasha12);

        // ==========meore
        if(level==2){
            pictures.set(0,R.drawable.ori1);
            pictures.set(1,R.drawable.ori2);
            pictures.set(2,R.drawable.ori3);
            pictures.set(3,R.drawable.ori4);
            pictures.set(4,R.drawable.ori5);
            pictures.set(5,R.drawable.ori6);
            pictures.set(6,R.drawable.ori7);
            pictures.set(7,R.drawable.ori8);
            pictures.set(8,R.drawable.ori9);
            pictures.set(9,R.drawable.ori10);
            pictures.set(10,R.drawable.ori11);
            pictures.set(11,R.drawable.ori12);

            picturesM.set(0,R.drawable.nori1);
            picturesM.set(1,R.drawable.nori2);
            picturesM.set(2,R.drawable.nori3);
            picturesM.set(3,R.drawable.nori4);
            picturesM.set(4,R.drawable.nori5);
            picturesM.set(5,R.drawable.nori6);
            picturesM.set(6,R.drawable.nori7);
            picturesM.set(7,R.drawable.nori8);
            picturesM.set(8,R.drawable.nori9);
            picturesM.set(9,R.drawable.nori10);
            picturesM.set(10,R.drawable.nori11);
            picturesM.set(11,R.drawable.nori12);
        }

        //mesame
//        else if(level==3){
//            pictures.set(0,R.drawable.sami1);
//            pictures.set(1,R.drawable.sami2);
//            pictures.set(2,R.drawable.sami3);
//            pictures.set(3,R.drawable.sami4);
//            pictures.set(4,R.drawable.sami5);
//            pictures.set(5,R.drawable.sami6);
//            pictures.set(6,R.drawable.sami7);
//            pictures.set(7,R.drawable.sami8);
//            pictures.set(8,R.drawable.sami9);
//            pictures.set(9,R.drawable.sami10);
//            pictures.set(10,R.drawable.sami11);
//            pictures.set(11,R.drawable.sami12);
//
//            picturesM.set(0,R.drawable.nasami1);
//            picturesM.set(1,R.drawable.nasami2);
//            picturesM.set(2,R.drawable.nasami3);
//            picturesM.set(3,R.drawable.nasami4);
//            picturesM.set(4,R.drawable.nasami5);
//            picturesM.set(5,R.drawable.nasami6);
//            picturesM.set(6,R.drawable.nasami7);
//            picturesM.set(7,R.drawable.nasami8);
//            picturesM.set(8,R.drawable.nasami9);
//            picturesM.set(9,R.drawable.nasami10);
//            picturesM.set(10,R.drawable.nasami11);
//            picturesM.set(11,R.drawable.nasami12);
//        }


        //============================
        imgBtn1=(ImageView)findViewById(R.id.button1);
        imgBtn2=(ImageView)findViewById(R.id.button2);
        imgBtn3=(ImageView)findViewById(R.id.button3);
        imgBtn4=(ImageView)findViewById(R.id.button4);
//
        imgBtn1_2=(ImageView)findViewById(R.id.button1_2);
        imgBtn2_2=(ImageView)findViewById(R.id.button2_2);
        imgBtn3_2=(ImageView)findViewById(R.id.button3_2);
        imgBtn4_2=(ImageView)findViewById(R.id.button4_2);

        imgs[0] = imgBtn1;
        imgs[1] = imgBtn2;
        imgs[2] = imgBtn3;
        imgs[3] = imgBtn4;

        imgBtn4.setTag(R.drawable.win1); //  //
        //=== aqedan

        nugeshi();
        imgBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isBackVisible1 && !setLeftIn.isRunning() && !setRightOut.isRunning()) {
                    // xelit random
                    if(pictureIndex>6 && level==1){
                        imgs[0].setImageResource(pictures.get(pictureIndex));
                        mogeba=1;
                    }
                    // -------------
                    setRightOut.setTarget(imgBtn1_2);
                    setLeftIn.setTarget(imgBtn1);
                    setRightOut.start();
                    setLeftIn.start();
                    isBackVisible1 = true;


                    if (mogeba == 1) {
//                        setRightOut.end();
//                        setLeftIn.end();
                        delay();
                    }

                    else if(guli==1) {
                        heartIsTaken = true;
                        heartOn(); // gulis amotrialeba
//                        Toast.makeText(getApplicationContext(), "You get heart: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                    }
                    else if(wageba==1 && heartIsTaken) {
                         heartOff();
                    }
                    else if(wageba==1 && !heartIsTaken) {
//                        Toast.makeText(getApplicationContext(), "You lose: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                        lose();
                    }
                }
            }
        });




        imgBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isBackVisible2 && !setLeftIn.isRunning() && !setRightOut.isRunning()) {
                    // xelit random
                    if(pictureIndex>6 && level==1){
                        imgs[1].setImageResource(pictures.get(pictureIndex));
                        mogeba=2;
                    }
                    // -------------
                    setRightOut.setTarget(imgBtn2_2);
                    setLeftIn.setTarget(imgBtn2);
                    setRightOut.start();
                    setLeftIn.start();
                    isBackVisible2 = true;
                    if( mogeba == 2) {
                       delay();

                    }
                    else if(guli==2) {
                        heartIsTaken = true;
                        heartOn(); // gulis amotrialeba
//                        Toast.makeText(getApplicationContext(), "You get heart: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                    }
                    else if(wageba==2 && heartIsTaken) {
                        heartOff();
                    }
                    else if(wageba==2 && !heartIsTaken) {
//                        Toast.makeText(getApplicationContext(), "You lose: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                        lose();
                    }
                }
            }
        });

        imgBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isBackVisible3 && !setLeftIn.isRunning() && !setRightOut.isRunning()) {
                    // xelit random
                    if(pictureIndex>6 && level==1){
                        imgs[2].setImageResource(pictures.get(pictureIndex));
                        mogeba=3;
                    }
                    // -------------
                    setRightOut.setTarget(imgBtn3_2);
                    setLeftIn.setTarget(imgBtn3);
                    setRightOut.start();
                    setLeftIn.start();
                    isBackVisible3 = true;
                    if( mogeba == 3) {
                        delay();

                    }
                    else if(guli==3) {
                        heartIsTaken = true;
                        heartOn(); // gulis amotrialeba
//                        Toast.makeText(getApplicationContext(), "You get heart: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                    }
                    else if(wageba==3 && heartIsTaken) {
                        heartOff();
                    }
                    else if(wageba==3 && !heartIsTaken) {
//                        Toast.makeText(getApplicationContext(), "You lose: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                        lose();
                    }
                }
            }
        });

        imgBtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isBackVisible4 && !setLeftIn.isRunning() && !setRightOut.isRunning()) {
                    // xelit random
                    if(pictureIndex>6 && level==1){
                        imgs[3].setImageResource(pictures.get(pictureIndex));
                        mogeba=4;
                    }
                    // -------------
                    setRightOut.setTarget(imgBtn4_2);
                    setLeftIn.setTarget(imgBtn4);
                    setRightOut.start();
                    setLeftIn.start();
                    isBackVisible4 = true;

                    if( mogeba == 4) {
                        delay();

                    }
                    else if(guli==4) {
                        heartIsTaken = true;
                        heartOn(); // gulis amotrialeba
//                        Toast.makeText(getApplicationContext(), "You get heart: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                    }
                    else if(wageba==4 && heartIsTaken) {
                        heartOff();
                    }
                    else if(wageba==4 && !heartIsTaken) {
//                        Toast.makeText(getApplicationContext(), "You lose: "+Integer.toString(pictureIndex+1),
//                                Toast.LENGTH_LONG).show();
                        lose();
                    }



//
                    Log.d("ye", Integer.toString((Integer) imgBtn4.getTag()));
                    Log.d("ye", Integer.toString(pictures.get(pictureIndex)));
                }
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        Toast.makeText(getApplicationContext(), "destroy: " ,
//                Toast.LENGTH_LONG).show();
        setRightOut.removeAllListeners();
        setLeftIn.removeAllListeners();
        fadeOut.cancel();
        shaker.cancel();
        pictures.clear();
        picturesM.clear();
        masivi=null;
        imgBtn1.setImageResource(android.R.color.transparent);
        imgBtn2.setImageResource(android.R.color.transparent);
        imgBtn3.setImageResource(android.R.color.transparent);
        imgBtn4.setImageResource(android.R.color.transparent);
        imgBtn1_2.setImageResource(android.R.color.transparent);
        imgBtn2_2.setImageResource(android.R.color.transparent);
        imgBtn3_2.setImageResource(android.R.color.transparent);
        imgBtn4_2.setImageResource(android.R.color.transparent);
        mPlayer1.stop();
        mPlayer2.stop();
        mPlayer3.stop();
        mPlayer4.stop();
        mPlayer5.stop();
        mPlayer6.stop();
        mPlayer7.stop();
        mPlayer8.stop();
        mPlayer9.stop();
        imgs=null;
    }

    public void nugeshi(){
        Log.d("kjkj", Integer.toString(pictures.size()));
        if(pictureIndex==11) { //11
           SharedPreferences.Editor editor = getSharedPreferences("levels", MODE_PRIVATE).edit();

            editor.putInt("level",++level );

            editor.commit();
            this.finish();
            return;
        }// 7 iyo
        pictureIndex++;
        Random r=new Random();
        int j,a,k,pey=0;
        j=r.nextInt(4)+1;
        do{
            a=r.nextInt(4)+1;
        }while(j==a);
        do{
            k=r.nextInt(4)+1;
        }while(k==a || k==j);
        do{
            pey++;
        }while(pey==a || pey==j || pey==k);

        heartIsTaken=false;

        wageba=a;
        guli=k;
        mogeba = pey;


        imgs[j-1].setImageResource(masivi[0]); // masivi[j-background1]   arafris surati
        imgs[a-1].setImageResource(masivi[1]); //masivi[a-background1]    wagebis surati
        imgs[k-1].setImageResource(masivi[2]); //masivi[k-background1]    es aris gilis surati
        imgs[pey-1].setImageResource(pictures.get(pictureIndex));



        imgBtn1_2.setImageResource(R.drawable.back1);
        imgBtn2_2.setImageResource(R.drawable.back2);
        imgBtn3_2.setImageResource(R.drawable.back3);
        imgBtn4_2.setImageResource(R.drawable.back4);

        // ?? ????? ????????? ???? ?????????? ?? ???? ???????? ??????? ?????????? ??????????? ????????? ?? ???? ???????????
        //-----------------

        setFirstImageBack();
        setSecondImageBack();
        setThierdImageBack();
        setFourthImageBack();
        //----------------

    }

    public void setFirstImageBack(){
        if( !setLeftIn.isRunning() && !setRightOut.isRunning()) {
            setRightOut.setTarget(imgBtn1);
            setLeftIn.setTarget(imgBtn1_2);
            setRightOut.start();
            setLeftIn.start();
            setRightOut.end();
            setLeftIn.end();
            isBackVisible1=false;

        }
    }

    public void setSecondImageBack(){
        if(!setLeftIn.isRunning() && !setRightOut.isRunning()) {
            setRightOut.setTarget(imgBtn2);
            setLeftIn.setTarget(imgBtn2_2);
            setRightOut.start();
            setLeftIn.start();
            setRightOut.end();
            setLeftIn.end();
            isBackVisible2=false;
        }
    }

    public void setThierdImageBack(){
        if( !setLeftIn.isRunning() && !setRightOut.isRunning()) {
            setRightOut.setTarget(imgBtn3);
            setLeftIn.setTarget(imgBtn3_2);
            setRightOut.start();
            setLeftIn.start();
            setRightOut.end();
            setLeftIn.end();
            isBackVisible3=false;
        }
    }

    public void setFourthImageBack(){
        if ( !setLeftIn.isRunning() && !setRightOut.isRunning()) {
            setRightOut.setTarget(imgBtn4);
            setLeftIn.setTarget(imgBtn4_2);
            setRightOut.start();
            setLeftIn.start();
            setRightOut.end();
            setLeftIn.end();
            isBackVisible4=false;
        }
    }

    public void back(){
        setFourthImageBack();
        setSecondImageBack();
        setThierdImageBack();
        setFirstImageBack();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==request_Code){
            Toast.makeText(getApplicationContext(), "You won: "+Integer.toString(pictureIndex+1),
                    Toast.LENGTH_LONG).show();


            nugeshi();
        }

        if(requestCode==getRequest_Code_wageba){
//            Toast.makeText(getApplicationContext(), "You won: "+Integer.toString(pictureIndex+1),
//                    Toast.LENGTH_LONG).show();
                  close();
        }
    }

    public void lose(){

        intentLOSE = new Intent(this, LoserActivity.class);
        Timer t =new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

        startActivityForResult(intentLOSE, getRequest_Code_wageba);
//        this.finish();
                    } //Closes run()

                }); //Closes runOnUiThread((){})
            }
        },500);
    }

    public void close(){
        this.finish();
    }

    public void delay(){
        // delay
        Timer t =new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {

                        Random rand = new Random();
                        int r = rand.nextInt(8) + 1;

                        mps.get(r).start();

                        intent.putExtra("winner", picturesM.get(pictureIndex));
                        intent.putExtra("pictureIndex", pictureIndex);
                        startActivityForResult(intent, request_Code);

                    } //Closes run()

                }); //Closes runOnUiThread((){})
            }
        }, 500);
    }

    public void heartOn(){
        imgHeartInside.setImageResource(R.drawable.hearton);
        imgHeartInside.setVisibility(View.VISIBLE);
        imgHeartInside.startAnimation(fadeOut);
    }

    public void heartOff(){
        imgHeartInside.setImageResource(R.drawable.heartoff);
        imgHeartInside.startAnimation(shaker);
    }


}






