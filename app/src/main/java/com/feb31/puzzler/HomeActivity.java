package com.feb31.puzzler;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


public class HomeActivity extends AppCompatActivity {

    private Tracker mTracker;

    InterstitialAd interstitial;

    SharedPreferences prefs;
    SharedPreferences prefsTapen;


    MediaPlayer mainPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        //=========================================== analitixs
        // Obtain the shared Tracker instance.
//
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //===========================================

        //====================  tapennnnnnnnnnn
        SharedPreferences.Editor editortapen = getSharedPreferences("tapen", MODE_PRIVATE).edit();
        editortapen.putBoolean("tapen", false);
        prefsTapen=getSharedPreferences("tapen", MODE_PRIVATE);
        editortapen.commit();
        //====================

        mainPlayer=MediaPlayer.create(this,R.raw.jazz);

        mainPlayer.start();
        mainPlayer.setLooping(true);

        prefs = getSharedPreferences("levels", MODE_PRIVATE);
// ---------------------------/.
    }


    @Override
    protected void onResume() {
        super.onResume();
        //==================================================== analitics
        mTracker.setScreenName("HomeScreen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        //====================================================
        // -------------------> reklama

//        ImageButton imageButton1=(ImageButton)findViewById(R.id.imageButton);
//        LinearLayout lhome=(LinearLayout)findViewById(R.id.home);
//        lhome.setBackgroundResource(R.drawable.bkg2);

        int loseCount=0;

        interstitial = new InterstitialAd(this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId("ca-app-pub-8024063655682792/1563711862");

        //Locate the Banner Ad in activity_main.xml

        SharedPreferences prefs = getSharedPreferences("reklama", MODE_PRIVATE);

//
        AdRequest adRequest = new AdRequest.Builder().build(); // shared

        loseCount=prefs.getInt("losecount", 0);
        Toast.makeText(this, Integer.toString(loseCount), Toast.LENGTH_LONG ).show(); /////


        if(loseCount!=0 && loseCount%5 == 0) {
            SharedPreferences.Editor editor = getSharedPreferences("reklama", MODE_PRIVATE).edit();
// ro ar ganmeordes
            editor.putInt("losecount",0 );

            editor.commit();
            interstitial.loadAd(adRequest);
        }

        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                interstitial.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("dasd", "asdasd");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }
        });
    }

    public void play(View view){

        if(prefs.getInt("level", 1)==3){ // 3 iyo
            Intent intent=new Intent(this,MoreGirls.class);
            startActivity(intent);
        }
        else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    public void showRoom(View view){
        Intent intent=new Intent(this,ShowRoomActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences.Editor editor = getSharedPreferences("reklama", MODE_PRIVATE).edit();

        editor.putInt("losecount", 0);
        editor.commit();

        mainPlayer.stop();
//===========




//============

    }
}
