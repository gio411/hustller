package com.feb31.puzzler;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class Interstitial extends Activity {

    InterstitialAd interstitial;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interstitial);
        interstitial = new InterstitialAd(Interstitial.this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId("ca-app-pub-8024063655682792/1563711862");

        //Locate the Banner Ad in activity_main.xml
//
        AdRequest adRequest = new AdRequest.Builder().build();
//fasfsafsa

        interstitial.loadAd(adRequest);


        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                // Call displayInterstitial() function
                interstitial.show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                Log.d("dasd", "asdasd");
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }
        });

    }
}
